export const NONE = Symbol('NONE');
export const READY = Symbol('READY');
export const SUCCESS = Symbol('SUCCESS');
export const ERROR = Symbol('ERROR');
