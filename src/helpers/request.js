import axios from 'axios';
import { GET } from '../constants/http-methods';
import { BASE_URL } from '@/constants/stackoverflow';

export const request = ({
  url, method = GET, params = {}, data = {}, ...props
}) => {
  return axios({
    url, method: method, params, data, ...props,
  })
    .then(response => response.data)
    .catch(err => Promise.reject(err));
};

export const requestStackoverflowApi = (action, params = {}) =>
  request({
    url: `${BASE_URL}/${action}`,
    params,
    headers: {
      'Content-type': 'application/x-www-form-urlencoded',
    },
  });
