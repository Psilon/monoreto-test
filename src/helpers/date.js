import zeroFill from 'zero-fill';

export const unixTimestampToTime = timestamp => {
  const date = new Date();
  date.setTime(timestamp * 1000);

  const month = zeroFill(2, date.getMonth() + 1);
  const day = zeroFill(2, date.getDate());
  const hours = zeroFill(2, date.getHours());
  const minutes = zeroFill(2, date.getMinutes());

  return `${date.getFullYear()}/${month}/${day} ${hours}:${minutes}`;
};
