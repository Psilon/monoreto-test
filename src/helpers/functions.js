export const isUndefined = value => typeof value === 'undefined';

export const isDefined = value => !isUndefined(value);

export const inArray = (array, value) => array.indexOf(value) !== -1;

export const objectGet = (object, key, defaultValue = null) =>
  (isDefined(object[key]) ? object[key] : defaultValue);
