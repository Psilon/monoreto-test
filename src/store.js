import Vue from 'vue';
import Vuex from 'vuex';
import isEmpty from 'lodash.isempty';
import { inArray } from '@/helpers/functions';
import { ERROR, NONE, READY, SUCCESS } from './constants/fetch-status';
import { apiSearchAnswers, apiSearchQuestion, apiSearchQuestions, apiSearchUserQuestions } from './api/stackoverflow';
import { isDefined, objectGet } from './helpers/functions';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    fetchStatus: NONE,
    fetchUserQuestionsStatus: NONE,
    queries: [],
    questions: [],
    answers: [],
    authorQuestions: {},
    authorId: null,
    isShowModalAuthorQueries: false,
  },

  getters: {
    isShowOverlay: ({isShowModalAuthorQueries}) => isShowModalAuthorQueries,
    isReady: ({fetchStatus}) => fetchStatus === READY,
    isReadyAuthorQuestions: ({fetchUserQuestionsStatus}) => fetchUserQuestionsStatus === READY,
    getQuestionById: ({ questions }) => id => questions.filter(question => id === question.question_id)[0],

    authorQuestions: ({authorQuestions}) => (authorId) => objectGet(authorQuestions, authorId, []),
    // authorQuestions ({authorQuestions, authorId}) {
    //   return objectGet(authorQuestions, authorId, []);
    // },
  },

  mutations: {
    updateFetchStatus (state, status) {
      state.fetchStatus = status;
    },
    updateFetchAuthorQuestionsStatus (state, status) {
      state.fetchUserQuestionsStatus = status;
    },
    addQuery (state, query) {
      if (query && !inArray(state.queries, query)) {
        state.queries.push(query);
      }
    },
    updateQuestions (state, items) {
      state.questions = items;
    },
    updateAuthorQuestions (state, {id, items}) {
      state.authorQuestions = {
        ...state.authorQuestions,
        [id]: items,
      };
    },
    updateAuthorId (state, id) {
      state.authorId = id;
    },
    updateAnswers (state, items) {
      state.answers = items;
    },
    hideAllModal (state) {
      state.isShowModalAuthorQueries = false;
    },
    hideModalAuthorQuestions (state) {
      state.isShowModalAuthorQueries = false;
    },
    showModalAuthorQuestions (state) {
      state.isShowModalAuthorQueries = true;
    },
  },

  actions: {
    searchQuestions ({state, commit}, query) {
      if (isEmpty(query)) {
        return;
      }

      commit('addQuery', query.q);
      commit('updateQuestions', []);
      commit('hideModalAuthorQuestions');
      commit('updateFetchStatus', READY);

      apiSearchQuestions(query)
        .then(({items}) => {
          commit('updateFetchStatus', SUCCESS);
          commit('updateQuestions', items);
        })
        .catch((err) => {
          commit('updateFetchStatus', ERROR);
          console.error(err);
        });
    },

    searchAnswers ({state, commit}, id) {
      if (!id) {
        return;
      }

      commit('updateFetchStatus', READY);
      commit('updateAnswers', []);
      commit('hideModalAuthorQuestions');

      apiSearchAnswers(id)
        .then(({items}) => {
          commit('updateFetchStatus', SUCCESS);
          commit('updateAnswers', items);
        })
        .catch((err) => {
          commit('updateFetchStatus', ERROR);
          console.error(err);
        });
    },

    searchQuestion ({state, commit}, id) {
      if (!id) {
        return;
      }

      commit('hideModalAuthorQuestions');
      commit('updateFetchStatus', READY);

      apiSearchQuestion(id)
        .then(({items}) => {
          commit('updateFetchStatus', SUCCESS);
          commit('updateQuestions', items);
        })
        .catch((err) => {
          commit('updateFetchStatus', ERROR);
          console.error(err);
        });
    },

    showAuthorQuestions ({state, commit}, id) {
      if (!id) {
        return;
      }

      commit('updateAuthorId', id);
      commit('showModalAuthorQuestions');

      if (isDefined(state.authorQuestions[id])) {
        return;
      }

      commit('updateFetchAuthorQuestionsStatus', READY);
      apiSearchUserQuestions(id)
        .then(({items}) => {
          commit('updateFetchAuthorQuestionsStatus', SUCCESS);
          commit('updateAuthorQuestions', {id, items});
        })
        .catch((err) => {
          commit('updateFetchAuthorQuestionsStatus', ERROR);
          console.error(err);
        });
    },
  },
});
