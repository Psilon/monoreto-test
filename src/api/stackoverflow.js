import Chain from 'middleware-chain-js';
// import pageMiddleware from './middlewares/page-middleware';
import orderMiddleware from './middlewares/order-middleware';
import pageSizeMiddleware from './middlewares/pagesize-middleware';
import sortMiddleware from './middlewares/sort-middleware';
import tokenMiddleware from './middlewares/token-middleware';
import filterMiddleware from './middlewares/filter-middleware';
import siteMiddleware from './middlewares/site-middleware';
import { requestStackoverflowApi } from '@/helpers/request';
import { ACTION_SEARCH } from '@/constants/stackoverflow';
import { TOKEN } from '../constants/stackoverflow';

const commonMiddlewares = [
  tokenMiddleware(TOKEN),
  filterMiddleware('withbody'),
  siteMiddleware,
];

export const apiSearchQuestions = query => {
  const chain = new Chain();
  chain.use([
    ...commonMiddlewares,
    orderMiddleware('desc'),
    sortMiddleware('creation'),
    pageSizeMiddleware(5),
  ]);
  chain.handle(query, query);

  return requestStackoverflowApi(ACTION_SEARCH, query);
};

export const apiSearchUserQuestions = id => {
  const query = {};

  const chain = new Chain();
  chain.use([
    ...commonMiddlewares,
    orderMiddleware('desc'),
    sortMiddleware('creation'),
    pageSizeMiddleware(5),
  ]);
  chain.handle(query, query);

  return requestStackoverflowApi(`users/${id}/questions`, query);
};

export const apiSearchAnswers = id => {
  const query = {};

  const chain = new Chain();
  chain.use([
    ...commonMiddlewares,
    orderMiddleware('desc'),
    sortMiddleware('votes'),
  ]);
  chain.handle(query, query);

  return requestStackoverflowApi(`questions/${id}/answers`, query);
};

export const apiSearchQuestion = id => {
  const query = {};

  const chain = new Chain();
  chain.use(commonMiddlewares);
  chain.handle(query, query);

  return requestStackoverflowApi(`questions/${id}`, query);
};
