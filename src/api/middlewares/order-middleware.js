// Set token is defined
export default (order = null) => (input = {}, output = {}, next) => {
  if (input.tagged && order && (order.toLowerCase() === 'desc' || order.toLowerCase() === 'asc')) {
    output.order = order;
  }
  next();
};
