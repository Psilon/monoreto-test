// Set token is defined
export default (sort = null) => (input = {}, output = {}, next) => {
  if (input.tagged && sort) {
    output.sort = sort;
  }

  next();
};
