// Set page
export default (page = null) => (input = {}, output = {}, next) => {
  if (page && page > 1) {
    output.page = page;
  }

  next();
};
