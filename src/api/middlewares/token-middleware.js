// Set token is defined
export default (token = null) => (input = {}, output = {}, next) => {
  if (token) {
    output.key = token;
  }

  next();
};
