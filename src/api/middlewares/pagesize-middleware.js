// Set page size
export default (pageSize = null) => (input = {}, output = {}, next) => {
  if (pageSize > 1) {
    output.pagesize = pageSize;
  }

  next();
};
