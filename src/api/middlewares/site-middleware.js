// Set site
export default (input = {}, output = {}, next) => {
  output.site = 'stackoverflow';

  next();
};
