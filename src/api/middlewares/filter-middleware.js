// Set token is defined
export default (filter = null) => (input = {}, output = {}, next) => {
  if (filter) {
    output.filter = filter;
  }
  next();
};
